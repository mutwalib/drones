#Run app

On commandline, navigate to the root of the project and type the following
```bash
mvn spring-boot:run
```

Or

Run it with visual studio code, check launch.json in .vscode folder.

#Assumptions

1: Battery levels drain by 50% after one delivery and need two minutes to recover

#Endpoints

##registering a drone;

Method:POST

url:http://localhost:8080/api/create

{"serialNumber": "","model": "Lightweight","weightLimit": 500,"batteryCapacity": 0,"state":"IDLE"}

reponse body will have a message

Returns 201 created if successful

##loading a drone with medication items;

Method:POST

url:http://localhost:8080/api/load

post a list of medication plus image files

[{"medication":{"name":"","weight":70, "code":"","drone":1},"mFile":{}}]

Returns 201 created if successful

##checking loaded medication items for a given drone; 

Method:GET

url:http://localhost:8080/api/checkitems/$id

##checking available drones for loading;

where 'id' identifies a drone

Method:GET

url:http://localhost:8080/api/checkavailability

##check drone battery level for a given drone;

returns 200 OK and list of available drone ids

Method:GET

url:http://localhost:8080/api/checkbattery/{{$id}}

returns a string of battery in percentage.
