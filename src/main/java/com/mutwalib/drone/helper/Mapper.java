package com.mutwalib.drone.helper;

import com.mutwalib.drone.dto.DroneDto;
import com.mutwalib.drone.dto.MedicationDto;
import com.mutwalib.drone.model.Drone;
import com.mutwalib.drone.model.Medication;

public class Mapper {
    public static DroneDto droneToDto(Drone drone) {
        DroneDto dto = new DroneDto();
        dto.setId(drone.getId());
        dto.setBatteryCapacity(drone.getBatteryCapacity());
        dto.setModel(drone.getModel());
        dto.setSerialNumber(drone.getSerialNumber());
        dto.setState(drone.getState());
        dto.setWeightLimit(drone.getWeightLimit());
        return dto;
    }

    public static MedicationDto MedicationToDto(Medication medication) {
        MedicationDto dto = new MedicationDto();
        dto.setId(medication.getId());
        dto.setCode(medication.getCode());
        dto.setImageUrl(medication.getImageUrl());
        dto.setLoadedAt(medication.getLoadedAt());
        dto.setName(medication.getName());
        dto.setWeight(medication.getWeight());
        return dto;
    }
}
