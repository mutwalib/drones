package com.mutwalib.drone.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mutwalib.drone.model.Loadings;

@Service
public interface LoadingsService {
    Loadings saveLoad(Loadings load);

    Loadings findLoadingById(Long id);

    Loadings findLoadingByDrone(Long droneId);

    List<Loadings> findAll();

}
