package com.mutwalib.drone.service;

import java.util.List;
import org.springframework.stereotype.Service;

import com.mutwalib.drone.model.Drone;

@Service
public interface DroneService {
    Drone saveDrone(Drone drone);

    List<Drone> findAll();

    String findBatteryLevel(Long id);
}
