package com.mutwalib.drone.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.mutwalib.drone.model.Medication;

@Service
public interface MedicationService {
    Medication saveMedication(Medication medication, MultipartFile image);

    List<Medication> checkLoadedItems(Long droneId);
}
