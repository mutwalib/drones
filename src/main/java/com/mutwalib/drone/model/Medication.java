package com.mutwalib.drone.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "medication")
@Builder
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "weight", nullable = false)
    private Integer weight;
    @Column(name = "code", nullable = false)
    private String code;
    @Column(name = "image_url", nullable = false)
    private String imageUrl;
    @Column(name = "load_at", nullable = false)
    private Calendar loadedAt = Calendar.getInstance();
    @ManyToOne
    @JoinColumn(name = "drone_id", referencedColumnName = "id", nullable = false)
    @JsonBackReference
    private Drone drone;

    public void setDroneId(Long droneId) {
        this.drone = new Drone();
        drone.setId(droneId);
    }
}
