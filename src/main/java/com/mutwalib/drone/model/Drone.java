package com.mutwalib.drone.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import com.mutwalib.drone.enumerations.ModelTypes;
import com.mutwalib.drone.enumerations.StateTypes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "drone")
@Builder
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Size(max = 100)
    @Column(name = "serial_number", nullable = false)
    private String serialNumber;
    @Column(name = "model", nullable = false)
    private ModelTypes model;
    @Column(name = "weight_limit", nullable = false)
    private String weightLimit;
    @Column(name = "battery_capacity", nullable = false)
    private String batteryCapacity;
    @Column(name = "state", nullable = false)
    private StateTypes state;
    @OneToMany(mappedBy = "drone", orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Medication> medication = new ArrayList<>();

    public List<Medication> getMedication() {
        return this.medication;
    }

    public void setMedication(List<Medication> medications) {
        if (medications == null) {
            if (this.medication != null) {
                ((Medication) this.medication).setDrone(null);
            }
        } else {
            for (Medication medication : medications) {
                medication.setDrone(this);
            }
        }
        this.medication = medications;
    }
}
