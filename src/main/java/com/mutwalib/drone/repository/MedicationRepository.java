package com.mutwalib.drone.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mutwalib.drone.model.Drone;
import com.mutwalib.drone.model.Medication;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    List<Medication> findByDrone(Drone drone);
}
