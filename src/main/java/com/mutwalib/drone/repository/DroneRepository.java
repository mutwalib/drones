package com.mutwalib.drone.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.mutwalib.drone.model.Drone;

public interface DroneRepository extends JpaRepository<Drone, Long> {
}