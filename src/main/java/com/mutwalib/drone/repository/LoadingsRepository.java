package com.mutwalib.drone.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mutwalib.drone.model.Loadings;

public interface LoadingsRepository extends JpaRepository<Loadings, Long> {
    Loadings findByDroneId(Long droneId);
}
