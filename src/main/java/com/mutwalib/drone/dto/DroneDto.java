package com.mutwalib.drone.dto;

import com.mutwalib.drone.enumerations.ModelTypes;
import com.mutwalib.drone.enumerations.StateTypes;
import com.mutwalib.drone.model.Drone;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class DroneDto {
    private Long id;
    private String serialNumber;
    private ModelTypes model;
    private String weightLimit;
    private String batteryCapacity;
    private StateTypes state;
}
