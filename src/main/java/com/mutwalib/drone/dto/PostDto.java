package com.mutwalib.drone.dto;

import org.springframework.web.multipart.MultipartFile;

import com.mutwalib.drone.model.Medication;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class PostDto {
    private Medication medication;
    private MultipartFile mfile;
}
