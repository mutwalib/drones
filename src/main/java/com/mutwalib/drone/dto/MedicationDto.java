package com.mutwalib.drone.dto;

import java.util.Calendar;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class MedicationDto {
    private Long id;
    private String name;
    private Integer weight;
    private String code;
    private String imageUrl;
    private Calendar loadedAt;
}
