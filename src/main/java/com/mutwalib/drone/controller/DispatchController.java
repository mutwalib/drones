package com.mutwalib.drone.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.mutwalib.drone.dto.DroneDto;
import com.mutwalib.drone.dto.MedicationDto;
import com.mutwalib.drone.dto.PostDto;
import com.mutwalib.drone.enumerations.ModelTypes;
import com.mutwalib.drone.helper.Mapper;
import com.mutwalib.drone.model.Drone;
import com.mutwalib.drone.model.Loadings;
import com.mutwalib.drone.model.Medication;
import com.mutwalib.drone.service.DroneService;
import com.mutwalib.drone.service.LoadingsService;
import com.mutwalib.drone.service.MedicationService;

@RestController
@RequestMapping("/api")
public class DispatchController {

    @Autowired
    private DroneService droneService;
    @Autowired
    private MedicationService medicationService;
    @Autowired
    private LoadingsService loadingsService;

    public DispatchController(DroneService droneService, MedicationService medicationService) {
        this.droneService = droneService;
        this.medicationService = medicationService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody Drone drone) {
        Drone droneResponse = new Drone();
        String message = "";
        HttpStatus status = HttpStatus.OK;
        int registered = droneService.findAll().size();
        if (registered < 10) {
            for (ModelTypes model : ModelTypes.values()) {
                if (model == drone.getModel()) {
                    droneResponse = droneService.saveDrone(drone);
                    message = "Drone saved successfully";
                    status = HttpStatus.CREATED;
                }
            }
            return new ResponseEntity<DroneDto>(Mapper.droneToDto(droneResponse), status);
        } else {
            status = HttpStatus.ALREADY_REPORTED;
            message = "Already registered 10 drones";
            return new ResponseEntity<>(message, status);
        }

    }

    @GetMapping("/getall")
    public List<DroneDto> getall() {
        List<Drone> list = droneService.findAll();
        List<DroneDto> dtos = new ArrayList<DroneDto>();
        for (Drone drone : list) {
            DroneDto dto = Mapper.droneToDto(drone);
            dtos.add(dto);
        }
        return dtos;
    }

    // Assumption 1: Drone takes 1 minute to load, 1 minute to be loaded, 1 minute
    // to deliver, 1 minute to be delivered, and 1 minute to return
    @PostMapping("/load")
    public ResponseEntity<?> loadDrone(@RequestBody List<PostDto> loads)
            throws JsonMappingException, JsonProcessingException {
        Integer weight = 0;
        Long droneId = 1L;
        for (PostDto load : loads) {
            MultipartFile image = load.getMfile();
            droneId = load.getMedication().getDrone().getId();
            Medication medication = load.getMedication();
            weight += medication.getWeight();
            if (weight <= 500)
                medicationService.saveMedication(medication, image);
            else
                return new ResponseEntity<>("Weight exceeded the weight limit", HttpStatus.BAD_REQUEST);
        }
        Loadings load = new Loadings();
        load.setLoadDate(Calendar.getInstance());
        load.setLoadsCount(loads.size());
        load.setDroneId(droneId);
        loadingsService.saveLoad(load);
        return new ResponseEntity<Medication>(HttpStatus.CREATED);
    }

    @GetMapping("/checkitems/{id}")
    public List<MedicationDto> checkMedicationItems(@PathVariable("id") Long droneId) {
        List<MedicationDto> dtos = new ArrayList<>();
        List<Medication> medications = medicationService.checkLoadedItems(droneId);
        for (Medication medication : medications) {
            MedicationDto dto = Mapper.MedicationToDto(medication);
            dtos.add(dto);
        }
        return dtos;
    }

    @GetMapping("/checkavailability")
    public ResponseEntity<?> checkAvailableDrone() {
        List<Long> allDroneId = new ArrayList<>();
        List<Long> loadedDroneIds = new ArrayList<>();
        List<Loadings> allLoadings = loadingsService.findAll();
        List<Drone> allDrones = droneService.findAll();
        for (Drone drone : allDrones) {
            allDroneId.add(drone.getId());
        }
        for (Loadings loading : allLoadings) {
            loadedDroneIds.add(loading.getDroneId());
        }
        return new ResponseEntity<>(
                allDroneId.stream().filter(element -> !loadedDroneIds.contains(element)).collect(Collectors.toList()),
                HttpStatus.OK);
    }

    // assumption 1: Battery levels drain by 50% after one delivery and need one
    // minute to recover
    @GetMapping("/checkbattery")
    public ResponseEntity<?> checkBatteryLevel(Long droneId) {
        String level = droneService.findBatteryLevel(droneId);
        String levelString = "";
        Loadings loadings = loadingsService.findLoadingByDrone(droneId);
        if (loadings != null) {
            Calendar loadDate = loadings.getLoadDate();
            Calendar now = Calendar.getInstance();
            Long diff = now.getTimeInMillis() - loadDate.getTimeInMillis();
            long diffInMinutes = diff / (60 * 1000) % 60;
            if (diffInMinutes < 2) {
                int batteryLevel = Integer.parseInt(level);
                int reducedLevel = batteryLevel - batteryLevel / 2;
                levelString = Integer.toString(reducedLevel) + "%";
            } else {
                levelString = level + "%";
            }
        }
        return new ResponseEntity<>(levelString, HttpStatus.OK);
    }
}