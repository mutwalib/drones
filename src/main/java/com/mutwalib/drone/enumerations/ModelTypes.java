package com.mutwalib.drone.enumerations;

public enum ModelTypes {
    Lightweight, Middleweight, Cruiserweight, Heavyweight
}
