package com.mutwalib.drone.enumerations;

public enum StateTypes {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
