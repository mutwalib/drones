package com.mutwalib.drone.serviceImp;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mutwalib.drone.model.Drone;
import com.mutwalib.drone.repository.DroneRepository;
import com.mutwalib.drone.service.DroneService;

@Service
public class DroneServiceImpl implements DroneService {

    @Autowired
    private DroneRepository droneRepository;

    public DroneServiceImpl(DroneRepository droneRepository) {
        this.droneRepository = droneRepository;
    }

    @Override
    public Drone saveDrone(Drone drone) {
        return droneRepository.save(drone);
    }

    @Override
    public List<Drone> findAll() {
        return droneRepository.findAll();
    }

    @Override
    public String findBatteryLevel(Long id) {
        Optional<Drone> drone = droneRepository.findById(id);
        if (drone.isPresent()) {
            String batteryLevel = drone.get().getBatteryCapacity();
            return batteryLevel;
        }
        return null;
    }

}
