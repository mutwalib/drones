package com.mutwalib.drone.serviceImp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.Deflater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.mutwalib.drone.model.Drone;
import com.mutwalib.drone.model.Medication;
import com.mutwalib.drone.repository.DroneRepository;
import com.mutwalib.drone.repository.MedicationRepository;
import com.mutwalib.drone.service.MedicationService;

@Service
public class MedicationServiceImpl implements MedicationService {
    @Autowired
    MedicationRepository medicationRepository;
    @Autowired
    DroneRepository droneRepository;
    @Value("${uploadDir}")
    private String uploadDir;
    private String originalFilename;
    private String originalFilename2;

    public MedicationServiceImpl(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    @Override
    public Medication saveMedication(Medication medication, MultipartFile image) {
        final String imagepath = uploadDir + "/uploads/";
        String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSSSSS").format(new Date());
        String imageName = "MED" + timestamp.replace(".", "").trim();
        originalFilename = image.getOriginalFilename();
        medication.setImageUrl(imagepath + imageName + "." + originalFilename
                .substring(originalFilename.lastIndexOf(".") + 1));
        uploadPhotos(image, imagepath, imageName);
        return medicationRepository.save(medication);
    }

    public void uploadPhotos(MultipartFile image, String realPath, String imageRenameString) {
        try {
            compressBytes(image.getBytes());
            originalFilename2 = image.getOriginalFilename();
            String filename = imageRenameString + "."
                    + originalFilename2.substring(originalFilename2.lastIndexOf(".") + 1);
            Files.copy(image.getInputStream(), Paths.get(realPath).resolve(filename));
        } catch (IOException e) {

        }
    }

    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        return outputStream.toByteArray();
    }

    @Override
    public List<Medication> checkLoadedItems(Long droneId) {
        Drone drone = droneRepository.findById(droneId).get();
        List<Medication> list = medicationRepository.findByDrone(drone);
        return list;
    }
}
