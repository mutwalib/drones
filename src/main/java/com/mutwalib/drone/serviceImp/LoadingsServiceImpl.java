package com.mutwalib.drone.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mutwalib.drone.model.Loadings;
import com.mutwalib.drone.repository.LoadingsRepository;
import com.mutwalib.drone.service.LoadingsService;

@Service
public class LoadingsServiceImpl implements LoadingsService {
    @Autowired
    private LoadingsRepository loadingsRepository;

    public LoadingsServiceImpl(LoadingsRepository loadingsRepository) {
        this.loadingsRepository = loadingsRepository;
    }

    @Override
    public Loadings saveLoad(Loadings load) {
        return loadingsRepository.save(load);
    }

    @Override
    public Loadings findLoadingById(Long id) {
        return loadingsRepository.getById(id);
    }

    @Override
    public Loadings findLoadingByDrone(Long droneId) {
        return loadingsRepository.findByDroneId(droneId);
    }

    @Override
    public List<Loadings> findAll() {
        return loadingsRepository.findAll();
    }
}
